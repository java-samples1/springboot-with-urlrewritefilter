springboot with URL Rewrite Filter
=======================================

springbootにURL Rewrite Filterを導入するサンプルです。


## 環境

* springboot: 2.3.0
* java: 8
* urlrewritefilter: 4.0.3

## 使い方 (Eclipse)

EGitパースペクティブでリモートリポジトリのクローン後、Mavenプロジェクトとしてインポートする。

* リポジトリのクローン
    - ローカルパスは適当なパスでいい(ex. `~/git/xxxx`)

* Mavenプロジェクトのインポート
    - リポジトリを選択し、右クリックメニューから「Mavenプロジェクトのインポート」を選択
    - 表示される「pom.xml」を選択してインポートする

* Mavenプロジェクトの更新
    - Javaパースペクティブを表示
    - Javaプログラムを選択し、右クリックメニューから「Maven」-「プロジェクトの更新」を選択

* プロジェクトの実行
    - Javaプログラムを選択し、右クリックメニューから「実行」-「Spring boot アプリケーション」を選択

## 機能

| エントリポイント(URL) |                                       説明                                       |
| --------------------- | -------------------------------------------------------------------------------- |
| `/sample/hello`       | RestControllerのメソッドに直接アクセス                                           |
| `/hello.html`         | URL Rewrite Filterによって`/sample/hello`にForwardされてRestControllerにアクセス |
