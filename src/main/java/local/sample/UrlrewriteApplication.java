package local.sample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UrlrewriteApplication {

	public static void main(String[] args) {
		SpringApplication.run(UrlrewriteApplication.class, args);
	}

}
