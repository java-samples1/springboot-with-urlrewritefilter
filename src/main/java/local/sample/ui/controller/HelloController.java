/**
 * 
 */
package local.sample.ui.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author t.fukai
 *
 */
@RestController
@RequestMapping("/sample")
public class HelloController {
	
	@RequestMapping(value = "/hello")
	private String hello() {
		return "Hello World!";
	}
}
